import pytest


def test_prima():
    print('test "prima" is started')
    assert 100 + 1 == 101


def includes_tip(bill, precentage):
    return bill*(100+precentage)/100


def test_tip_100():
    print("Tipping 20% on 100 euros")
    assert includes_tip(100, 20) == 120
