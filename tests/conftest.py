import pytest
from fastapi.testclient import TestClient
from v4_02_auth.main import app

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from v4_02_auth.models import Base
from v4_02_auth.database import get_db
from v4_02_auth import models

# Bad Practice to put it here
Other_DATABASE_URL = 'postgresql://postgres:root@localhost/test_api'

# Running engine for ORM translation (python to SQL)
database_engine = create_engine(Other_DATABASE_URL)
# Template for the connection
testing_SessionTemplate = sessionmaker(
    autocommit=False, autoflush=False, bind=database_engine)


def override_get_db():
    db = testing_SessionTemplate()
    try:
        yield db
    finally:
        db.close()


@pytest.fixture()
def session():
    Base.metadata.drop_all(bind=database_engine)
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db
    # the fixture will define somecodes/ to be pre-required


@pytest.fixture()
def create_user(client):
    user_credential = {
        'email': 'test.user-creation@domain.com', 'password': '111111'}
    res = client.post('/users', json=user_credential)
    new_user = res.json()
    new_user['password'] = user_credential['password']
    return new_user


@pytest.fixture()
def client(session):
    yield TestClient(app)


@pytest.fixture()
def user_token(create_user, client):
    res = client.post(
        '/auth/', data={'username': create_user['email'], 'password': create_user['password']})
    return res.json().get('access_token')


@pytest.fixture()
def authorized_client(client, user_token):
    client.headers = {**client.headers,
                      'Authorization': f"Bearer {user_token}"}
    return client
