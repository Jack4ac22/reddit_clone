def test_docs(client):
    test_reponse = client.get('/docs')
    assert test_reponse.status_code == 200


def test_redoc(client):
    test_reponse = client.get('/redoc')
    assert test_reponse.status_code == 200


