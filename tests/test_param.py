import pytest


def total_with_tip(bill, percentage):
    if (bill < 0 or percentage < 0):
        raise Exception("Bill and/or percentage should be positive")
    return bill + bill*percentage/100

# parametrization

















@pytest.mark.parametrize('num1, num2, expectation', [(10, 20, 12), (100, 20, 120)])
def test_tip_something(num1, num2, expectation):
    assert total_with_tip(num1, num2) == expectation
