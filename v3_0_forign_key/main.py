import psycopg2
from psycopg2.extras import RealDictCursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel
from typing import Optional

try:
    db_conn = psycopg2.connect(
        host="localhost",
        database="one_to_many",
        user='postgres',
        password='root',
        cursor_factory=RealDictCursor
    )
    cursor = db_conn.cursor()
    print(f"connected to db")
except Exception as error:
    print('dataBase connection faild')
    print(f'eroor is: {error}')


class BlogPost(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None
    user_id: int


app = FastAPI()


#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")
def test():
    return {"data": "This is the test message"}


# GET that rerive all blog_posts
@app.get("/posts")
def get_posts():
    cursor.execute("SELECT * FROM blog_posts")
    database_posts = cursor.fetchall()
    return {'data': database_posts}


# POST /posts  ***CREATE NEW BLOGPOST***
# POST /posts endpoint
@app.post("/posts", status_code=status.HTTP_201_CREATED)
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
    # cursor.execute("SELECT * FROM users WHERE users.id = %s;",
    #                (str(new_post.user_id),))
    # corresponding_post = cursor.fetchone()
    # if not corresponding_post:
    #     raise HTTPException(
    #         status_code=status.HTTP_404_NOT_FOUND,
    #         detail=f"There is no user with the given id:{new_post.user_id}"
    #     ) #alternative solution is next:
    try:
        cursor.execute("INSERT INTO public.blog_posts(title, content, published, user_id) VALUES (%s, %s, %s, %s) RETURNING *;",
                       (new_post.title, new_post.content, new_post.published, new_post.user_id))
        post_dict = cursor.fetchone()
        db_conn.commit()  # commiting the chenges
   # Alternative solution to change the HTTP code
        return {"data": post_dict}
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail=f"There is no user with the given id:{new_post.user_id}"
                            )


# GET /posts/{id_param}  ***GET POST WITH ID***
# {id_param}  is the path parameter
@app.get('/posts/{id_param}', status_code=status.HTTP_200_OK)
def get_post(id_param: int, response: Response):
    cursor.execute(
        "SELECT * FROM blog_posts WHERE blog_posts.id = %s;", (id_param,))
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}


# DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_param: int):
    # deleting post logic
    # 1. find the post
    cursor.execute("SELECT * FROM blog_posts WHERE blog_posts.id = %s;",
                   (str(id_param),))
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
        # 2. Remove element from array
    cursor.execute(
        "DELETE FROM blog_posts WHERE blog_posts.id = %s;", (str(id_param),))
    db_conn.commit()

    # Best practice HTTP CODE 204 = not content response
    return Response(status_code=status.HTTP_204_NO_CONTENT)


# PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    # cursor.execute("SELECT * FROM blog_posts WHERE blog_posts.id = %s;",
    #                (str(id_param),))
    # corresponding_post = cursor.fetchone()
    # if not corresponding_post:
    #     raise HTTPException(
    #         status_code=status.HTTP_404_NOT_FOUND,
    #         detail=f"Not corresponding post was found with id:{id_param}"
    #     ) #ulternative solution would be by using the exceptions
    try:
        cursor.execute("SELECT * FROM users WHERE users.id = %s;",
                       (str(updated_post.user_id),))
        corresponding_post = cursor.fetchone()
        if not corresponding_post:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"There is no user with the given id:{updated_post.user_id}"
            )
        post_dict = cursor.execute("UPDATE public.blog_posts SET title=%s, content=%s, published=%s, user_id = %s WHERE id = %s RETURNING *;",
                                   (updated_post.title, updated_post.content, updated_post.published, updated_post.user_id, id_param))
        post_dict = cursor.fetchone()
        db_conn.commit()

        return post_dict
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail=f"There is no user with the given id:{updated_post.user_id}"
                            )
    # except: #use te exeption to check the post exsisting
