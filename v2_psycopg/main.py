import psycopg2
# it allows to add name to the field of the cursor.
from psycopg2.extras import RealDictCursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields

try:
    db_conn = psycopg2.connect(
        host="localhost",
        database="Reddit Clone",
        user='postgres',
        password='root',
        cursor_factory=RealDictCursor
    )
    cursor = db_conn.cursor()
    print(f"connected to db")
except Exception as error:
    print('dataBase connection faild')
    print(f'eroor is: {error}')


class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None


# DATA #####: Array of Blog Posts (Local list)
my_posts = [
    {"id": 1, "title": "Welcome to our blog",
        "content": "This is the beginning..."},
    {"id": 2, "title": "Top 10 best activities in Luxembourg",
        "content": "Our list of..."}
]

# find and return the post with the a given i


def find_post(given_id):
    for post in my_posts:
        if post["id"] == given_id:
            return post

# Find and return the index of a specific post


def find_post_index(given_id):
    for index, post in enumerate(my_posts):
        if post["id"] == given_id:
            return index


###### FastAPI instance name ######
app = FastAPI()


#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response


# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    cursor.execute("SELECT * FROM posts")
    database_posts = cursor.fetchall()
    return {'data': database_posts}


# POST /posts  ***CREATE NEW BLOGPOST***
# POST /posts endpoint
@app.post("/posts", status_code=status.HTTP_201_CREATED)
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
    cursor.execute("INSERT INTO public.posts(title, content, published) VALUES (%s, %s, %s) RETURNING *;",
                   (new_post.title, new_post.content, new_post.published))
    post_dict = cursor.fetchone()
    db_conn.commit()  # commiting the chenges
   # Alternative solution to change the HTTP code
    return {"data": post_dict}


# GET /posts/trending  ***GET LATEST BLOGPOST***
@app.get('/posts/trending')                                # Path order matters
def trending_posts():
    return {"data": my_posts[len(my_posts)-1]}


# GET /posts/{id_param}  ***GET POST WITH ID***
# {id_param}  is the path parameter
@app.get('/posts/{id_param}', status_code=status.HTTP_200_OK)
def get_post(id_param: int, response: Response):
    cursor.execute("SELECT * FROM posts WHERE posts.id = %s;", (id_param,))
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}


# DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_param: int):
    # deleting post logic
    # 1. find the post
    cursor.execute("SELECT * FROM posts WHERE posts.id = %s;", (str(id_param),))
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
        # 2. Remove element from array
    cursor.execute("DELETE FROM posts WHERE posts.id = %s;", (str(id_param),))
    db_conn.commit()

    # Best practice HTTP CODE 204 = not content response
    return Response(status_code=status.HTTP_204_NO_CONTENT)


# PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    cursor.execute("SELECT * FROM posts WHERE posts.id = %s;", (str(id_param),))
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    post_dict = cursor.execute("UPDATE public.posts SET title=%s, content=%s, published=%s WHERE id = %s RETURNING *;",
                               (updated_post.title, updated_post.content, updated_post.published, id_param))
    post_dict = cursor.fetchone()
    db_conn.commit()

    return post_dict
